# TP-Kotlin

Ce TP va vous permettre de découvrir l'univers de Kotlin, ses spécificités ainsi que les coroutines tout en (re)découvrant Android et son IDE pas lourd du tout.  
Marche à suivre : chaque questions correspondent à une classe dans le projet, vous devez y remplir les méthodes ou le code manquant.

## Pré-requis

Attention à l'espace disque.
![](Tuto GitHub ?)

Lire ce petit encart avant de lancer Android Studio :wink:.

![](img/FilAndroidTuto.png)

### Petite infos

Ce shortcut très important : `ctrl + alt + l` pour formater votre code.

Si vous voulez connecter votre téléphone Android à Android Studio, suivez [ce petit tuto](https://www.frandroid.com/comment-faire/tutoriaux/229753_questcequelemodedebogageusb) et autorisez le partage de fichier quand vous connectez votre téléphone.

## Partie 1: Java versus Kotlin

![](img/JavaVersusKotlin.png)

### Surcharge de fonctions

En Java, il est possible de déclarer plusieurs fonctions avec le même nom si ils ont des paramètres différents (voir fichier `QuestionUneJava.java`).

En Kotlin, il est possible de faire autrement, d'une manière plus légère, subtile et élégante :smirk:.  
Remplissez le fichier `QuestionUneKotlin.kt` en utilisant les `arguments nommés` (Named Arguments) et les `arguments par défaut` (Default Arguments).
Pour savoir si vous avez résolu la question, exécutez son test correspondant dans le Package de tests `com.ifi.tp.kotlin`, à savoir le fichier `TestsQuestionUneJavaKotlin.kt`.

Clique droit - Run pour lancer le test.

### Smart Cast

Les marcheurs blanc arrivent...:cold_sweat:  
Les chevaliers du Gondor, arrivés la veille dans les contrées froides du Nord doivent réunir leurs chevaliers pour combattre à leurs côtés.  
Les Chevaliers riches doivent donc réunir leurs pauvres chevaliers pour qu'ils combattent pour eux (pendant qu'ils mangent une petite glace sur le Mur).

Pareil que la question précédente, vous devez simplifier le code Java dans le fichier `QuestionDeuxJava.java` et écrire le code Kotlin simplifié dans le fichier `QuestionDeuxKotlin.kt`. Vous devez utiliser les smart cast et interfaces. En utilisant le shortcut `Format Code` et en ne mettant *aucun espaces* entre les différents composants (la fonction et les classes/l'interface), **mon nombre de ligne est de 9**, rapprochez vous en le plus possible. Sinon le Gondor risque de perdre...:scream:

Lancez les tests du fichier `TestsQuestionDeuxJavaKotlin.kt` pour vérifier que tout fonctionne.

## Partie 2: coroutines et compagnie

![](img/oliverEtCompagnie.jpg)

Il vous sera demandé de développer une application Android pour crypter des images. Les coroutines seront utilisées afin de voir si ces coroutines permettent d'accélérer l'exécution de certains processus mais aussi pour découvrir leur fonctionnement (techno très récente - 2017/2018).

Pour avancer dans cette partie du TP, il suffit de reprendre généralement le code de la question d'avant et de le coller dans le prochain fichier de classe.

### Architecture du Code

L'application peut être lancée en runnant le fichier `MainActivity.kt`. Dans celui-ci, des Buttons ont été initialisés avec des listeners.   
Vous devez donc remplir la class correspondant aux différentes question tout en faisant attention que le contenu du listener s'adapte à votre code.

### Question 1: Vos premiers pas...

Vous devez crypter l'image appelé `test.png` situé dans les `Res` du projet Android en utilisant l'opération XOR avec des nombres Randoms.  
Attention, le temps de la fonction est affiché dans la ListView `timeListe`, pensez donc bien à remplir les différentes propriétés...

Aide :  
* Copy mutable
* `val timeTransform = measureTimeMillis...` pour mesurer le temps

### Question 2: la Clé

![](img/key.jpg)

Idem que la Q1, sauf qu'ici vous devez garder les nombres Randoms généré dans un tableau dans votre Class. Pour ensuite générer une image de pixel random, et la sauvegarder.

Décrypter l'image avec son image clé pour bien vérifier que cela fonctionne (en utilisant le Bouton reverse).

N'oubliez pas d'afficher le temps de la fonction dans la ListView `timeList`.

### Question 3: votre première fois... :trollface:

Maintenant, utilisez UNE coroutine pour optimiser (ou pas :innocent:) le temps d'exécution du cryptage de l'image.
Affichez le temps de la fonction dans la ListView `timeList` et vérifier que le reverse fonctionne avant de passer à la question suivante.

### Question 4: multiple coroutines, easy

Faire en sorte que l'utilisateur puisse rentré un nombre dans le champ `Input Text` prévu à cette effet, puis lancer la fonction de cryptage avec X coroutines; X étant le nombre présent dans l'`Input Text`.  
Décrypter l'image comme dans Q2 pour bien vérifier que cela fonctionne.  
Affiché le temps de la fonction dans la ListView `timeList`.  
Essayer de découvrir le nb de coroutines optimal.

### Question 5: vos propres images

Pour upload une image dans la Galerie de votre téléphone/émulateur :

    adb shell mkdir /sdcard/Pictures
    adb push mypic.jpg /sdcard/Pictures

Faite en sorte qu'on puisse décrypter l'image et cliquer sur le bouton `Question 5` pour pouvoir cypter l'image.

### Question 6 :godmode:

Permettre de choisir son image clé pour crypter l'image voulue.
