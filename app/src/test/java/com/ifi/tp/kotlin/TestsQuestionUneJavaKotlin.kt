package com.ifi.tp.kotlin

import com.ifi.tp.kotlin.QuestionsKotlin.makeReservationsExamples
import org.junit.Assert
import org.junit.Test

class TestsQuestionUneJavaKotlin {
    @Test
    fun makeReservation_nominal() {
        Assert.assertEquals(listOf("Drinks:Jean1", "Drinks:Paul6", "GrosMorfal1", "Jacky2"), makeReservationsExamples())
    }
}