package com.ifi.tp.kotlin

import com.ifi.tp.kotlin.QuestionsKotlin.PoorKnight
import com.ifi.tp.kotlin.QuestionsKotlin.RichKnight
import com.ifi.tp.kotlin.QuestionsKotlin.makeReservationsExamples
import com.ifi.tp.kotlin.QuestionsKotlin.preparationFight
import org.junit.Assert
import org.junit.Test

class TestsQuestionDeuxJavaKotlin {
    @Test
    fun testPoorKnight_nominal() {
        val power = 0
        val pk = PoorKnight(power)
        Assert.assertEquals(power, preparationFight(pk))
    }

    @Test
    fun testRichKnight_nominal() {
        val power = 5
        val pk = PoorKnight(power)
        Assert.assertEquals(power, preparationFight(pk))
    }

    @Test
    fun testRichKnight_multiple() {
        Assert.assertEquals(10, preparationFight(RichKnight(RichKnight(PoorKnight(3), PoorKnight(4)),PoorKnight(3))))
    }
}