package com.ifi.tp.kotlin.QuestionsCoroutines

import android.graphics.Bitmap
import kotlinx.coroutines.runBlocking
import kotlin.properties.Delegates
import kotlin.system.measureTimeMillis

open class Question(bm: Bitmap) {

    var bitmapToTransform: Bitmap = bm
    var bitmapTransformed: Bitmap by Delegates.notNull()


    open fun reponse() = runBlocking {
    }

    fun reponseReverse(keyTab: IntArray, imgPixels: IntArray) = runBlocking {
        val bm = bitmapToTransform.copy(bitmapToTransform.config, true)


        for (i in 0..(imgPixels.size - 1)) {

            var pixelToTransform = imgPixels[i] xor keyTab[i]

            imgPixels[i] = pixelToTransform
        }
        bm.setPixels(imgPixels, 0, bm.width, 0, 0, bm.width, bm.height)
        bitmapTransformed = bm
    }
}
