package com.ifi.tp.kotlin.QuestionsCoroutines

import android.graphics.Bitmap
import kotlinx.coroutines.runBlocking
import kotlin.properties.Delegates
import kotlin.system.measureTimeMillis

class QuestionUne : Question {

    constructor(bm: Bitmap) : super(bm)

    var time: Long by Delegates.notNull()

    override fun reponse() = runBlocking {
        val bm = bitmapToTransform.copy(bitmapToTransform.config, true)

        var h = bm.height
        var w = bm.width

        val pixels = IntArray(h * w)

        bm.getPixels(pixels, 0, w, 0, 0, w, h)

        val timeTransform = measureTimeMillis {

            for (i in 0..(pixels.size - 1)) {
                var pixelToTransform = pixels[i] xor (10..999999999).random()

                pixels[i] = pixelToTransform
            }
        }

        bm.setPixels(pixels, 0, bm.width, 0, 0, bm.width, bm.height)
        bitmapTransformed = bm

        time = timeTransform
    }
}
