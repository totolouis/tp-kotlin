package com.ifi.tp.kotlin

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import com.ifi.tp.kotlin.QuestionsCoroutines.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.FileNotFoundException
import kotlin.properties.Delegates


class MainActivity : AppCompatActivity() {

    var times: MutableList<String> = ArrayList()
    private var key: IntArray by Delegates.notNull()
    private var imgPixels: IntArray by Delegates.notNull()
    private var personnalImg: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var adapterTime = ArrayAdapter<String>(this@MainActivity, android.R.layout.simple_list_item_1, times)
        listTime.setAdapter(adapterTime)

        // Crypter image avec nombre random XOR
        btQuestion1.setOnClickListener {
            btReverse.isEnabled = false
            val q1 = QuestionUne(BitmapFactory.decodeResource(resources, R.drawable.imagemystere))

            q1.reponse()

            times.add("q1 " + q1.time.toString())
            adapterTime.notifyDataSetChanged()

            imageView.setImageBitmap(q1.bitmapTransformed)
            personnalImg = false
        }

        // Generer image clé et redecrypter l'image
        btQuestion2.setOnClickListener {
            btReverse.isEnabled = true
            val q2 = QuestionDeux(BitmapFactory.decodeResource(resources, R.drawable.imagemystere))

            q2.reponse()
            key = q2.keysPixels
            imgPixels = q2.imgPixels
            times.add("q2 " + q2.time.toString())
            adapterTime.notifyDataSetChanged()

            imageView.setImageBitmap(q2.bitmapTransformed)
            personnalImg = false
        }

        // Utiliser une coroutine
        btQuestion3.setOnClickListener {
            btReverse.isEnabled = true

            val q3 = QuestionTrois(BitmapFactory.decodeResource(resources, R.drawable.imagemystere))
            q3.reponse()
            key = q3.keysPixels
            imgPixels = q3.imgPixels
            times.add("q3 " + q3.time.toString())
            adapterTime.notifyDataSetChanged()

            imageView.setImageBitmap(q3.bitmapTransformed)
            saveImage(q3.keysPicture, "image key")
            personnalImg = false
        }


        // Utiliser plusieurs coroutines
        btQuestion4.setOnClickListener {
            if (numberCoroutine.text.toString() != "" && numberCoroutine.text.toString().toInt() > 0) {
                btReverse.isEnabled = true
                val q4 = QuestionQuatre(BitmapFactory.decodeResource(resources, R.drawable.imagemystere), numberCoroutine.text.toString().toInt())

                q4.reponse()
                key = q4.keysPixels
                imgPixels = q4.imgPixels
                times.add("q4 " + q4.time.toString())
                adapterTime.notifyDataSetChanged()

                imageView.setImageBitmap(q4.bitmapTransformed)
                saveImage(q4.keysPicture, "image key")
                personnalImg = false
            }
        }

        // Button reverse pour afficher la véritable image
        btReverse.setOnClickListener {
            var q: Question by Delegates.notNull()
            when (personnalImg) {
                true -> q = Question((imageView.drawable as BitmapDrawable).bitmap)
                false -> q = Question(BitmapFactory.decodeResource(resources, R.drawable.imagemystere))
            }
            q.reponseReverse(key, imgPixels)

            imageView.setImageBitmap(q.bitmapTransformed)
            btReverse.isEnabled = false
        }

        // Permets d'ouvrir la galerie pour sélectionner l'image que l'on veut
        btGetImage.setOnClickListener {
            btReverse.isEnabled = true
            getImage()
            personnalImg = true
            btQuestion5.isEnabled = true
        }

        // Lance la Q5 avec l'image choisie
        btQuestion5.setOnClickListener {
            btReverse.isEnabled = true
            val q5 = QuestionCinq((imageView.drawable as BitmapDrawable).bitmap)

            q5.reponse()
            key = q5.keysPixels
            imgPixels = q5.imgPixels
            times.add("q5 " + q5.time.toString())
            adapterTime.notifyDataSetChanged()

            imageView.setImageBitmap(q5.bitmapTransformed)
            saveImage(q5.keysPicture, "image key")
        }

        // Add a listener sur un bt Q6, en reutilisant l'image upload du getImage...

    }

    // Permets de sauvegarder une image
    private fun saveImage(bm: Bitmap, title: String) {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), 1)
        }

        var savedImageURL: String? = null

        try {
            // Save image to gallery
            savedImageURL = MediaStore.Images.Media.insertImage(
                    contentResolver,
                    bm,
                    title,
                    "Image of $title"
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // Parse the gallery image url to uri
        Log.d("uri", savedImageURL)
    }

    // Permets d'ouvrir une Activity Galerie pour choisir l'image qu'on veut utiliser
    private fun getImage() {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, 1)
    }

    // Permets de capté l'image choisie dans l'Activity Galerie
    override fun onActivityResult(reqCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(reqCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            try {
                val imageUri = data!!.data
                val imageStream = contentResolver.openInputStream(imageUri!!)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
                imageView.setImageBitmap(selectedImage)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
            }

        } else {
            Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show()
        }
    }
}








