package com.ifi.tp.kotlin.QuestionsCoroutines

import android.graphics.Bitmap
import android.util.Log
import kotlinx.coroutines.*
import kotlin.properties.Delegates
import kotlin.system.measureTimeMillis

class QuestionQuatre : Question {

    constructor(bm: Bitmap, coroutines: Int = 5) : super(bm) {
        numberCoroutines = coroutines
    }

    var keysPixels: IntArray by Delegates.notNull()
    var imgPixels: IntArray by Delegates.notNull()

    var time: Long by Delegates.notNull()

    var numberCoroutines: Int by Delegates.notNull()
    var keysPicture: Bitmap by Delegates.notNull()

    // Utilisation des co-routines
    override fun reponse() = runBlocking {
        val bm = bitmapToTransform.copy(bitmapToTransform.config, true)

        var h = bitmapToTransform.height
        var w = bitmapToTransform.width

        var pixels = IntArray(h * w)
        var keys = IntArray(h * w);

        bm.getPixels(pixels, 0, w, 0, 0, w, h)

        time = measureTimeMillis {
            val result = cryptImageWithMultipleCoroutines(numberCoroutines, pixels, keys)
            pixels = result.first
            keys = result.second
        }
        keysPixels = keys
        imgPixels = pixels
        bm.setPixels(pixels, 0, w, 0, 0, w, h)
        bitmapTransformed = bm

        Log.d("EXEC TIME FUNCTION", time.toString())

        // Enregister l'image de keys dans le fs de l'utilisateur
        bm.setPixels(keys, 0, w, 0, 0, w, h)
        keysPicture = bm
    }

    private suspend fun cryptImageWithMultipleCoroutines(numberCoroutines: Int, pixels: IntArray, keys: IntArray): Pair<IntArray, IntArray> {
        // Init
        val jobs: MutableList<Job> = ArrayList()
        var iteration = 0
        var size = pixels.size / numberCoroutines
        var reste = pixels.size % numberCoroutines
        var valueToPass: MutableList<Pair<Int, Int>> = ArrayList()

        // On remplit une liste de Pair qui stocke les limites du range quand on va crée la coroutine (vu que c'est async, c'est du hasard si on stocke pas)
        for (i in 0..(numberCoroutines - 1)) {
            valueToPass.add(i, Pair(iteration, iteration + size - 1))
            when (i < numberCoroutines - 1) {
                true -> iteration += size
                false -> {
                    iteration += reste
                    size = reste
                }
            }
        }

        // On parcourt la liste et on lance la coroutine
        valueToPass.forEach { it ->
            jobs.add(GlobalScope.launch {
                for (j in it.first..it.second) {
                    // On prends le pixel pour le XOR
                    val pixel = pixels[j]
                    keys[j] = (10..999999999).random()
                    var pixelToTransform = pixel xor keys[j]

                    // On le stock
                    pixels[j] = pixelToTransform
                }
            })
        }

        // On attends que les jobs soient terminés
        jobs.joinAll()

        return Pair(pixels, keys)
    }

}
