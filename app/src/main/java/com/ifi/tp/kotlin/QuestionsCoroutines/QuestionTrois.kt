package com.ifi.tp.kotlin.QuestionsCoroutines

import android.graphics.Bitmap
import kotlinx.coroutines.*
import kotlin.properties.Delegates
import kotlin.system.measureTimeMillis

class QuestionTrois : Question {

    constructor(bm: Bitmap) : super(bm)

    var keysPicture: Bitmap by Delegates.notNull()
    var keysPixels: IntArray by Delegates.notNull()
    var imgPixels: IntArray by Delegates.notNull()

    var time: Long by Delegates.notNull()

    override fun reponse() = runBlocking {
        val bm = bitmapToTransform.copy(bitmapToTransform.config, true)

        var h = bm.height
        var w = bm.width

        var pixels = IntArray(h * w)
        var keys = IntArray(h * w);

        bm.getPixels(pixels, 0, w, 0, 0, w, h)

        var timeTransform = measureTimeMillis {
            val result = cryptImage(pixels, keys)
            pixels = result.first
            keys = result.second
        }

        bm.setPixels(pixels, 0, w, 0, 0, w, h)
        bitmapTransformed = bm

        // Enregister l'image de keys dans le fs de l'utilisateur
        bm.setPixels(keys, 0, w, 0, 0, w, h)
        keysPicture = bm
        keysPixels = keys
        imgPixels = pixels
        time = timeTransform
    }

    private suspend fun cryptImage(pixels: IntArray, keys: IntArray): Pair<IntArray, IntArray> {
        val jobs: MutableList<Job> = ArrayList()

        jobs.add(GlobalScope.launch {
            for (j in 0..pixels.size / 2) {
                val pixel = pixels[j]
                keys[j] = (10..999999999).random()
                var pixelToTransform = pixel xor keys[j]

                pixels[j] = pixelToTransform
            }
        })

        jobs.add(GlobalScope.launch {
            for (j in pixels.size / 2..(pixels.size - 1)) {
                val pixel = pixels[j]
                keys[j] = (10..999999999).random()
                var pixelToTransform = pixel xor keys[j]

                pixels[j] = pixelToTransform
            }
        })

        jobs.joinAll()

        return Pair(pixels, keys)
    }

}
