package com.ifi.tp.kotlin.QuestionsJava;

public class QuestionUneJava {

    public String makeReservation(String name, Boolean willEat, int numberOfPlace){
        return (willEat ? name : "Drinks : " + name) + numberOfPlace;
    }

    public String makeReservation(String name, int numberOfPlace){
        return makeReservation(name, false, numberOfPlace);
    }

    public String makeReservation(String name, Boolean willEat){
        return makeReservation(name, willEat, 1);
    }

    public String makeReservation(String name){
        return makeReservation(name, 1);
    }
}
