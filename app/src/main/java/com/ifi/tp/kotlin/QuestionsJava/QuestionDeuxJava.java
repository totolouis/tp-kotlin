package com.ifi.tp.kotlin.QuestionsJava;

public class QuestionDeuxJava {
    public int preparationFight(Knight k) {
        if (k instanceof PoorKnight) {
            return ((PoorKnight) k).getPower();
        }
        if (k instanceof RichKnight) {
            RichKnight rk = (RichKnight) k;
            return preparationFight(rk.getKnightBestFriend()) + preparationFight(rk.getKnightSecondBestFriend());
        }
        throw new IllegalArgumentException("Unknown expression");
    }
}

interface Knight{
}

class PoorKnight implements Knight{

    int power = 0;

    public PoorKnight(int power){
        this.power = power;
    }

    int getPower(){
        return power;
    }
}

class RichKnight implements Knight{

    public RichKnight(Knight k1, Knight k2){
        this.KnightBestFriend = k1;
        this.KnightSecondBestFriend = k2;
    }

    Knight KnightBestFriend;
    Knight KnightSecondBestFriend;

    Knight getKnightBestFriend(){
        return KnightBestFriend;
    }

    Knight getKnightSecondBestFriend(){
        return KnightSecondBestFriend;
    }
}
