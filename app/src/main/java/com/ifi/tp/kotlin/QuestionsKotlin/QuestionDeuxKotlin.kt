package com.ifi.tp.kotlin.QuestionsKotlin

// Les marcheurs blancs arrivent :peur:

fun preparationFight(k: Knight): Int =
        when (k) {
            is PoorKnight -> k.power
            is RichKnight -> preparationFight(k.KnightBestFriend) + preparationFight(k.KnightSecondBestFriend)
            else -> throw IllegalArgumentException("Unknown expression")
        }


interface Knight
class PoorKnight(val power: Int = 0) : Knight
class RichKnight(val KnightBestFriend: Knight, val KnightSecondBestFriend: Knight) : Knight

