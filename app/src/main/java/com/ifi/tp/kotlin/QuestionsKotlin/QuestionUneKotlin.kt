package com.ifi.tp.kotlin.QuestionsKotlin

// Faire une fonction qui a le même comportement que les 4 fonctions dans QuestionUneJava.java

// Utilisez une template String pour affiché le mot "Drinks :" avant le nom de la personne.
fun makeReservation(name: String, willEat: Boolean = false, numberOfPlace: Int = 1) =
        (if (willEat) name else "Drinks:$name") + numberOfPlace


fun makeReservationsExamples() = listOf(
        // Exemple avec name
        makeReservation("Jean"),
        // Exemple avec name et place
        makeReservation("Paul", numberOfPlace = 6),
        // Exemple avec name, boolean
        makeReservation("GrosMorfal", willEat = true),
        // Exemple avec name, boolean et place
        makeReservation(name = "Jacky", numberOfPlace = 2, willEat = true)
)