package com.ifi.tp.kotlin.QuestionsCoroutines

import android.graphics.Bitmap
import kotlinx.coroutines.runBlocking
import kotlin.properties.Delegates
import kotlin.system.measureTimeMillis

class QuestionDeux : Question {

    constructor(bm: Bitmap) : super(bm)

    var keysPixels: IntArray by Delegates.notNull()
    var imgPixels: IntArray by Delegates.notNull()
    var time: Long by Delegates.notNull()

    override fun reponse() = runBlocking {
        var bm = bitmapToTransform.copy(bitmapToTransform.config, true)

        var h = bm.height
        var w = bm.width

        val pixels = IntArray(h * w)
        val keys = IntArray(h * w);

        bm.getPixels(pixels, 0, w, 0, 0, w, h)

        val timeTransform = measureTimeMillis {

            for (i in 0..(pixels.size - 1)) {

                // Genere un nombre random pour crypter l'image
                keys[i] = (10..999999999).random()
                var pixelToTransform = pixels[i] xor keys[i]


                pixels[i] = pixelToTransform
            }
        }

        bm.setPixels(pixels, 0, w, 0, 0, w, h)
        bitmapTransformed = bm

        // Enregister l'image de keys dans le fs de l'utilisateur
        bm.setPixels(keys, 0, w, 0, 0, w, h)
        keysPixels = keys
        imgPixels = pixels
        time = timeTransform
    }


}



